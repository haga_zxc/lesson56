package kg.attractor.tasks.services;

import kg.attractor.tasks.DTO.FullTaskDTO;
import kg.attractor.tasks.DTO.TaskDTO;
import kg.attractor.tasks.model.Task;
import kg.attractor.tasks.model.User;
import kg.attractor.tasks.repository.TaskRepository;
import kg.attractor.tasks.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

@Service
public class TaskService {
    private final UserRepository ur;
    private final TaskRepository tr;

    public TaskService(UserRepository ur, TaskRepository tr) {
        this.ur = ur;
        this.tr = tr;
    }


    public Slice<TaskDTO> findAllTasks(Pageable pageable,Authentication authentication) {
        var user = (User)authentication.getPrincipal();
        String id = user.getId();
        var order = tr.findAllByUser_Id(id, pageable);
        return order.map(TaskDTO::from);
    }

        public FullTaskDTO addTask(FullTaskDTO fullTaskDTO,Authentication authentication){
            var user = (User)authentication.getPrincipal();
            Task task = Task.builder()
                    .title(fullTaskDTO.getTitle())
                    .description(fullTaskDTO.getDescription())
                    .end(fullTaskDTO.getTime())
                    .user(user)
                    .status(fullTaskDTO.getStatus())
                    .build();
            tr.save(task);
            return FullTaskDTO.from(task);
        }

        public FullTaskDTO changeStatus(String id){
        String status = "";
        var task = tr.findById(id);
        if(task.get().getStatus().equals("New")){
            status =  "In Work";
        }else if(task.get().getStatus().equals("In Work")){
            status =  "End";
        }else{
            status = "End";
        }
        tr.delete(task.get());
        task.get().setStatus(status);
        tr.save(task.get());
        return FullTaskDTO.from(task.get());
    }
    public FullTaskDTO getTask(String id){
        var task = tr.findById(id);
        return FullTaskDTO.from(task.get());
    }
}
