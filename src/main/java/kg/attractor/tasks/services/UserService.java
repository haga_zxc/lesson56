package kg.attractor.tasks.services;

import kg.attractor.tasks.DTO.FullUserDTO;
import kg.attractor.tasks.DTO.UserDTO;
import kg.attractor.tasks.model.User;
import kg.attractor.tasks.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository ur;

    public UserService(UserRepository ur) {
        this.ur = ur;
    }


    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = ur.findAll(pageable);
        return slice.map(UserDTO::from);
    }

    public FullUserDTO registerUser(FullUserDTO fullUserDTO) {
        var user = User.builder()
                .id(fullUserDTO.getId())
                .name(fullUserDTO.getName())
                .email(fullUserDTO.getEmail())
                .password(fullUserDTO.getPassword())
                .build();
        ur.save(user);
        return FullUserDTO.from(user);
    }
}
