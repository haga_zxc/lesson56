package kg.attractor.tasks.utils;

import kg.attractor.tasks.model.Task;
import kg.attractor.tasks.model.User;
import kg.attractor.tasks.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class InitDataBase {
    private static final Random r = new Random();

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, TaskRepository taskRepository) {
        return (args) -> {
            userRepository.deleteAll();
            taskRepository.deleteAll();

            var demoUser = User.makeUser();
            demoUser.setEmail("123@gmail.com");             //Username for Spring Security for test the program
            demoUser.setName("Arstan");
            demoUser.setPassword("qwerty");                  //Password for Spring Security for test the program
            userRepository.save(demoUser);

            List<User> users = Stream.generate(User::makeUser)
                    .limit(10)
                    .collect(toList());
            userRepository.saveAll(users);

            List<Task> tasks = Stream.generate(() -> Task.makeTask(users.get(r.nextInt(users.size()))))
                    .limit(10)
                    .collect(toList());
            taskRepository.saveAll(tasks);

        };
    }
}
