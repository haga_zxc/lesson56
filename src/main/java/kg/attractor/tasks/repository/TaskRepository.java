package kg.attractor.tasks.repository;

import kg.attractor.tasks.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface TaskRepository extends PagingAndSortingRepository<Task, String> {
    Page<Task> findAllByUser_Id(String id, Pageable pageable);

}
