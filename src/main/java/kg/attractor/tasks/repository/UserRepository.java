package kg.attractor.tasks.repository;

import kg.attractor.tasks.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, String> {
    Optional<User> findByEmail(String email);
}
