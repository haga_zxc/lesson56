package kg.attractor.tasks.controller;

import kg.attractor.tasks.DTO.FullUserDTO;
import kg.attractor.tasks.DTO.UserDTO;
import kg.attractor.tasks.anotation.ApiPageable;
import kg.attractor.tasks.services.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService us;

    public UserController(UserService us) {
        this.us = us;
    }

    @ApiPageable
    @GetMapping
    public Slice<UserDTO> findUsers(@ApiIgnore Pageable pageable) {
        return us.findUsers(pageable);
    }

    @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public FullUserDTO addNewUser(@RequestBody FullUserDTO fullUserDTO) {
        return us.registerUser(fullUserDTO);
    }

}
