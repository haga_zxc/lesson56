package kg.attractor.tasks.controller;

import kg.attractor.tasks.DTO.FullTaskDTO;
import kg.attractor.tasks.DTO.TaskDTO;
import kg.attractor.tasks.anotation.ApiPageable;
import kg.attractor.tasks.services.TaskService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final TaskService ts;

    public TaskController(TaskService ts) {
        this.ts = ts;
    }

    @ApiPageable
    @GetMapping
    public Slice<TaskDTO> findAllTasks(@ApiIgnore Pageable pageable, Authentication authentication) {
        return ts.findAllTasks(pageable, authentication);
    }

    @GetMapping("/change_id/{id}")
    public FullTaskDTO changeStatus(@PathVariable("id") String id) {
        return ts.changeStatus(id);
    }

    @GetMapping("/{id}")
    public FullTaskDTO getTask(@PathVariable("id") String id) {
        return ts.getTask(id);
    }

    @PostMapping(path = "/registerTask", consumes = MediaType.APPLICATION_JSON_VALUE)
    public FullTaskDTO addTask(@RequestBody FullTaskDTO fullTaskDTO, Authentication authentication) {
        return ts.addTask(fullTaskDTO, authentication);
    }
}
