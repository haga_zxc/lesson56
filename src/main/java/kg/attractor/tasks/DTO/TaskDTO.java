package kg.attractor.tasks.DTO;

import kg.attractor.tasks.model.Task;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class TaskDTO {
    public static TaskDTO from(Task task) {
        return builder()
                .id(task.getId())
                .title(task.getTitle())
                .status(task.getStatus())
                .time(task.getEnd())
                .userId(task.getUser().getId())
                .build();
    }

    private String id;
    private String title;
    private String status;
    private String userId;
    private LocalDateTime time;
}
