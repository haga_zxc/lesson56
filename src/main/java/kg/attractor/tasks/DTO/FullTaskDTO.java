package kg.attractor.tasks.DTO;

import kg.attractor.tasks.model.Task;
import kg.attractor.tasks.model.User;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class FullTaskDTO {
    public static FullTaskDTO from(Task task) {
        return builder()
                .id(task.getId())
                .title(task.getTitle())
                .status(task.getStatus())
                .time(task.getEnd())
                .user(task.getUser())
                .description(task.getDescription())
                .build();
    }

    private String id;
    private String title;
    private String description;
    private String status;
    private User user;
    private LocalDateTime time;
}
