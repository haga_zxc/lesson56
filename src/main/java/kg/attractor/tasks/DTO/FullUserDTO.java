package kg.attractor.tasks.DTO;

import kg.attractor.tasks.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class FullUserDTO {
    public static FullUserDTO from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();


    }

    private String id;
    private String name;
    private String email;
    private String password;
}