package kg.attractor.tasks.model;

import kg.attractor.tasks.utils.Generator;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

@Document
@Data
@Builder
public class Task {
    private static final Random r = new Random();
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String title;
    private String description;
    private LocalDateTime end;
    @DBRef
    private User user;
    private String status;

    private static LocalDateTime getDate() {

        return LocalDateTime.of((2020 + r.nextInt(2)), (4 + r.nextInt(5)),
                (1 + r.nextInt(20)), (1 + r.nextInt(15)), (r.nextInt(50)));
    }

    private static String makeStatus() {
        int x = r.nextInt(3);
        String status = "";
        if (x == 0) {
            status = "New";
        } else if (x == 1) {
            status = "In Work";
        } else {
            status = "End";
        }
        return status;
    }

    public static Task makeTask(User user) {
        return builder()
                .title(Generator.makeName())
                .description(Generator.makeDescription())
                .end(getDate())
                .status(makeStatus())
                .user(user)
                .build();
    }
}
